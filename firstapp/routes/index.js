const express = require('express');
const { query, validationResult, body } = require('express-validator');
const flash = require('connect-flash');

const router = express.Router();
router.use(express.urlencoded({ extended: true }), flash());

const controller = require('../controllers/notices.controller');

/* GET home page. */
router.get('/', controller.get);

const nonEmptyTitle = () => body('title').trim().notEmpty().escape();
const nonEmptyContent = () => body('content').trim().notEmpty().escape();
const nonEmptyName = () => body('name').trim().notEmpty().escape();
const tenDigitPhoneNumber = () => body('number').escape().trim().isNumeric().isLength({ min: 10, max: 10 });

router.post(
  '/',
  nonEmptyTitle(),
  nonEmptyContent(),
  nonEmptyName(),
  tenDigitPhoneNumber(),
  controller.post);

module.exports = router;
