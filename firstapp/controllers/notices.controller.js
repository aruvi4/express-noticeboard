const service = require('../services/notices.service');
const NOTICES_LOCALSTORE_PATH = __dirname + '/../storage/notices.json';

const { query, validationResult } = require('express-validator');

function get(req, res, next) {
    let errors = req.flash('errors');
    console.log(errors);
    res.render('index', {
        title: 'Noticeboard',
        notices: service.getNotices(NOTICES_LOCALSTORE_PATH),
        errors: errors
    });
}

function post(req, res, next) {
    console.log(req.body);
    const result = validationResult(req);
    if (!result.isEmpty()) {
      console.log("there are problems");
      req.flash('errors', result.array());
      res.redirect(303, '/');
      return;
    }
    var notice = {
      title: req.body.title,
      content: req.body.content,
      contact: {
        name: req.body.name,
        number: req.body.number
      }
    };
    service.addNotice(notice, notices_localstore_path);
    res.redirect(303, '/');
}

module.exports = {get, post};