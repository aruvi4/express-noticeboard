const fs = require("fs");

var notices = [];

function getNotices(path) {
    read(path);
    return notices;
}

function shiftAndPush(array, item) {
    var temp = array.slice(1, array.length);
    temp.push(item);
    return temp;
}

function addNotice(notice, path) {
    if (notices.length < 6) {
        notices.push(notice);
    }
    else {
        notices = shiftAndPush(notices, notice);
    }
    write(path);
}

function write(path) {
    fs.writeFileSync(path, JSON.stringify(notices), {encoding: 'utf8'});
}

function read(path) {
    var file = fs.readFileSync(path, notices, {encoding: 'utf8'});
    notices = JSON.parse(file);
}

module.exports = {getNotices, addNotice};