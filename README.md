# To set up an express server from scratch:

1. Install node and npm if you don't have them already.
1. Refer to the Express Getting Started guide. Always refer to the official getting started guide. [https://expressjs.com/en/starter/installing.html](Getting Started Guide)
1. Create an express application with pugjs as the view engine (This is not necessary; you can use any view engine you like; you can eschew view engines altogether and use a reactjs frontend in which case you have to rewrite the server to send JSON responses rather than render views.)
1. The validation uses [https://express-validator.github.io/docs/guides/getting-started/](express-validator). Make sure to install it and make it a dependency in `package.json`
1. The Post-Redirect-Get pattern involves using session 'flash' variables as a way to communicate validation failures from the failing validation in POST to the page that is rendered by GET. For this, you need two things:
    1. express-session: add `const session = require('express-session')` which might require you to `npm install express-session` and configure as `app.use(session({ cookie: { maxAge: 60000 }, secret: 'testing'}));`.
    1. connect-flash: add `const flash = require('connect-flash');` and configure as `router.use(<your other handlers>, flash());`.

